# SPDX-FileCopyrightText: 2020-2024 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

SUMMARY = "mGBA is a Game Boy Advance, Game Boy Color, and Game Boy emulator."
DESCRIPTION = "mGBA is an emulator for running Game Boy Advance games. It aims \
to be faster and more accurate than many existing Game Boy Advance emulators, \
as well as adding features that other emulators lack. It also supports Game Boy \
and Game Boy Color games."
HOMEPAGE = "https://mgba.io/"
BUGTRACKER = "https://github.com/mgba-emu/mgba/issues"

SECTION = "emulator"

LICENSE = "MPL-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=815ca599c9df247a0c7f619bab123dad"

DEPENDS = "virtual/libsdl2"

SRCBRANCH = "0.10"
SRC_URI = "git://github.com/mgba-emu/mgba.git;protocol=https;branch=${SRCBRANCH}"
SRCREV = "1c61b54208ca6266129d0f2394c04bd8c44f98c5"
S = "${WORKDIR}/git"

inherit cmake pkgconfig

PACKAGECONFIG_GL ?= "${@bb.utils.filter('DISTRO_FEATURES', 'opengl', d)}"
PACKAGECONFIG ??= "${PACKAGECONFIG_GL}"
PACKAGECONFIG[gles2]  = "-DBUILD_GLES2=ON,-DBUILD_GLES2=OFF,virtual/libgles2"
PACKAGECONFIG[opengl] = "-DBUILD_GL=ON,-DBUILD_GL=OFF,virtual/libgl"
PACKAGECONFIG[qt]     = "-DBUILD_QT=ON,-DBUILD_QT=OFF,qtbase-native qtbase"

EXTRA_OECMAKE = " \
    -DBUILD_SDL=ON \
\
    -DBUILD_GLES3=OFF \
    -DENABLE_SCRIPTING=OFF \
    -DUSE_DEBUGGERS=OFF \
    -DUSE_DISCORD_RPC=OFF \
    -DUSE_ELF=OFF \
    -DUSE_FFMPEG=OFF \
    -DUSE_LIBZIP=OFF \
    -DUSE_LZMA=OFF \
    -DUSE_MINIZIP=OFF \
    -DUSE_PNG=OFF \
    -DUSE_SQLITE3=OFF \
    -DUSE_ZLIB=OFF \
"

# QA: Avoid creation of redundant RPATH /usr/lib
EXTRA_OECMAKE += "-DCMAKE_SKIP_RPATH=TRUE"

FILES:${PN} += "${datadir}/icons"
