# SPDX-FileCopyrightText: 2020-2021 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

SUMMARY = "SMS Plus is a Sega Master System and Game Gear emulator."
DESCRIPTION = "This is a modernized, cleaned up, basic, standalone port of \
Charles MacDonald's SMS Plus. The goal of this project is to be useful to \
homebrewers or emulator developers who need a simple reference Sega Master \
System/Game Gear emulator for testing."
HOMEPAGE = "https://github.com/0ldsk00l/smsplus"
BUGTRACKER = "https://github.com/0ldsk00l/smsplus/issues"

SECTION = "emulator"

LICENSE = "LicenseRef-SMS-Plus"
LIC_FILES_CHKSUM = "file://LICENSE;md5=17c21dbe23cd4103f6213145cfd7dc01"
NO_GENERIC_LICENSE[LicenseRef-SMS-Plus] = "LICENSE"

DEPENDS = "glfw libepoxy"

SRCBRANCH = "master"
SRC_URI = "git://github.com/0ldsk00l/smsplus.git;protocol=https;branch=${SRCBRANCH}"
SRCREV = "c8df1d8bed4ef376527006497b8dbd899e32aec2"
S = "${WORKDIR}/git"

inherit pkgconfig

EXTRA_OEMAKE = " \
    CC='${CC}' \
    CFLAGS='${CFLAGS}' \
    LDFLAGS='${LDFLAGS}' \
"

do_install() {
    install -d ${D}/${bindir}
    install -m 0755 smsplus ${D}/${bindir}
}
