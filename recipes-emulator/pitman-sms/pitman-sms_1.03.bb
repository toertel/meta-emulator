# SPDX-FileCopyrightText: 2021-2022 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Pitman SMS by under4mhz"
DESCRIPTION = "A fun puzzle game where you maneuver the boulders so you can \
collect the diamonds. Collect all the diamonds to move to the next level. When \
stuck, press the right fire button to restart the level."
HOMEPAGE = "https://www.smspower.org/Homebrew/Pitman-SMS"
BUGTRACKER= "https://www.smspower.org/forums/18141-Pitman"

SECTION = "emulator"

LICENSE = "CLOSED"

SRC_URI = "https://www.smspower.org/uploads/Homebrew/Pitman-SMS-${PV}.zip"
#SRC_URI[md5sum] = "1e029277d331523962fe089709ba3726"
SRC_URI[sha256sum] = "59b306181c692ed7b90d2956645894452e5bb86bc6a0d7b7b0d5de5371f39567"

inherit allarch

do_install() {
    install -d ${D}/${datadir}/games/sms
    install -m 0644 ${WORKDIR}/Pitman.sms ${D}/${datadir}/games/sms/pitman.sms
}

FILES:${PN} = "${datadir}/games/sms"
