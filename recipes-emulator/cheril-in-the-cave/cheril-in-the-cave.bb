# SPDX-FileCopyrightText: 2020-2022 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Cheril in the Cave by The Mojon Twins for NES"
DESCRIPTION = "'Great reward for capturing the evil monster in the cave', \
Cheril read in a notice placed in her local bulletin board. 'Watermelon candy \
for life!''. So she put on her pink catsuit and rode her pink motorcycle at \
full speed to deal with such evil. And to get some candy. \
Control Cheril using the D-pad. Jump using the A button. Use the B button to \
interact with stuff (read signs, activate switches, pick up objects ...). \
Cheril can only carry one object at once, so be careful and work it out."
HOMEPAGE = "https://www.mojontwins.com/juegos_mojonos/cheril-in-the-cave/"
BUGTRACKER= "https://github.com/mojontwins/Cave_NES/issues"

SECTION = "emulator"

LICENSE = "CLOSED"

SRC_URI = "https://www.mojontwins.com/juegos/mojon-twins--cave-cnrom.nes"
SRC_URI[md5sum] = "1e029277d331523962fe089709ba3726"
SRC_URI[sha256sum] = "b17032c5e5aa110c237d3714d271e7591a9fd003cdef318f9e167688ea697f48"

inherit allarch

do_install() {
    install -d ${D}/${datadir}/games/nes
    install -m 0644 ${WORKDIR}/mojon-twins--cave-cnrom.nes ${D}/${datadir}/games/nes/cheril-in-the-cave.nes
}

FILES:${PN} = "${datadir}/games/nes"
