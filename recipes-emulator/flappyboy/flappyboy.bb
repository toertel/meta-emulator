# SPDX-FileCopyrightText: 2024 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Flappy Bird clone for the Nintendo Game Boy."
HOMEPAGE = "https://github.com/bitnenfer/FlappyBoy"
BUGTRACKER= "https://github.com/bitnenfer/FlappyBoy/issues"

SECTION = "emulator"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=cc1ff13e46d922405cd1add5ec53b9f4"

SRC_URI = "git://github.com/bitnenfer/FlappyBoy.git;protocol=https;branch=master"
SRCREV = "e976de17bfc74d08169e84797ac0f0c639c7a810"
S = "${WORKDIR}/git"

inherit allarch

do_compile[noexec] = "1"

do_install() {
    install -d ${D}/${datadir}/games/gameboy
    install -m 0644 ${WORKDIR}/git/build/gameboy/flappyboy.gb ${D}/${datadir}/games/gameboy/flappyboy.gb
}

FILES:${PN} = "${datadir}/games/gameboy"
