# SPDX-FileCopyrightText: 2020-2022 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Nestopia is a Nintendo Entertainment System (Famicon) emulator."
DESCRIPTION = "Nestopia was designed to emulate the NES / Famicon hardware as \
accurately as possible. Thus, it has a little higher hardware requirements as \
other NES emulators."
HOMEPAGE = "https://github.com/0ldsk00l/nestopia"
BUGTRACKER = "https://github.com/0ldsk00l/nestopia/issues"

SECTION = "games"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=686e6cb566fd6382c9fcc7a557bf4544"

DEPENDS = "virtual/libsdl2 zlib libarchive libepoxy"

SRCBRANCH = "master"
SRC_URI = "git://github.com/0ldsk00l/nestopia.git;protocol=https;branch=${SRCBRANCH}"
SRCREV = "793c70fc7e79e187d8353adf95f9cfe77e6a80b3"
S = "${WORKDIR}/git"

inherit autotools pkgconfig

FILES:${PN} += "${datadir}/icons"
