# SPDX-FileCopyrightText: 2020-2022 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://directfbrc"

do_install:append() {
    install -d ${D}/${sysconfdir}
    install -m 644 ${WORKDIR}/directfbrc ${D}/${sysconfdir}
}
