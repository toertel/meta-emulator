<!--
SPDX-FileCopyrightText: 2020-2025 Mark Jonas <toertel@gmail.com>

SPDX-License-Identifier: MIT
-->

[![Pipeline status](https://gitlab.com/toertel/meta-emulator/badges/kirkstone/pipeline.svg)](https://gitlab.com/toertel/meta-emulator/pipelines)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/toertel/meta-emulator)](https://api.reuse.software/info/gitlab.com/toertel/meta-emulator)

# meta-emulator

This meta-layer supplies recipes for compiling various emulators with The Yocto Project.

The meta-layer is has been tested on Poky's core-image-minimal using qemux86-64 with The Yocto Project 4.0 "Kirkstone".

> :warning: **WARNING**
> Unfortunately Ubuntu 24.04 uses an AppArmor configuration which is not
> compatible with BitBake. See [Ubuntu Launchpad bug #2056555](https://bugs.launchpad.net/ubuntu/+source/apparmor/+bug/2056555)
> and [Workaround for uid_map error on Ubuntu 24.04](https://lists.yoctoproject.org/g/yocto/topic/106192359#)
> for details.
>
> If you know what you are doing you can either temporarily or permanently
> disable Ubuntu 24.04's AppArmor user namespace creation restrictions.
>
> A) Run `sudo sysctl kernel.apparmor_restrict_unprivileged_userns=0`
>    to temporarily work around the problem until you reboot.
>
> B) Run `echo 'kernel.apparmor_restrict_unprivileged_userns = 0' | sudo tee /etc/sysctl.d/20-apparmor-donotrestrict.conf`
>    and reboot for permanently allowing unprivileged user namespaces.

## Setup

For setting up the build and documenting how to build different configurations [kas](https://github.com/siemens/kas) is used. Please read and follow the [kas dependencies & installation](https://kas.readthedocs.io/en/latest/userguide.html#dependencies-installation) for installing kas.

## Build

Create a directory `yocto` (or any other name of your choice) and clone meta-emulator into it.

```
mkdir yocto
cd yocto
git clone https://gitlab.com/toertel/meta-emulator.git
```

After cloning you can switch to a different branch in case you do not want to build the default version.

```
cd meta-emulator
git checkout kirkstone
cd ..
```

The following emulators are available.

| Emulator | DirectFB (framebuffer) | X11            |
| -------- | -----------------------| -------------- |
| mGBA     | mgba-directfb-*        | mgba-x11-*     |
| Nestopia | nestopia-directfb-*    | nestopia-x11-* |
| SMS Plus | n/a                    | smsplus-x11-*  |

Here, an example how to build an image with mGBA using DirectFB on Kirkstone.

```
kas build meta-emulator/kas/mgba-directfb-kirkstone.yaml
```

## Run

None of the emulators is running flawlessly so far. Help is appreciated.

| Emulator | DirectFB (framebuffer)               | X11                            |
| -------- | ------------------------------------ | ------------------------------ |
| mGBA     | Starts but screen stays black        | Runs but no sound              |
| Nestopia | misses libGL.so.1 or libOpenGL.so.0  | Runs but no sound              |
| SMS Plus | n/a                                  | Starts, shows Pitman under4mhz logo, then freezes |

### DirectFB (framebuffer)

Grant X server access to all local users on the system

> :warning: **WARNING**
> On Ubuntu 24.04 running QEMU with an SDL or GTK+ frontend is at least tricky.
> Here, graphics is shown using the `publicvnc` option of *runqemu*. You can
> connect to the VNC server on 127.0.0.1:5900.
>
> Precondition for using `sdl` or `gtk` instead of `publicvnc` is to grant
> X server access to all local users on the system by running `xhost +local:`.

Start QEMU with the image previously built. Here, an example for *mgba*.

```
kas shell -c "runqemu kvm slirp serial audio publicvnc" meta-emulator/kas/mgba-directfb-kirkstone.yaml
```

Login as user *root* and and start `mgba`.

```
mgba /usr/share/games/gameboy/flappyboy.gb
```

### X11

> :warning: **WARNING**
> How to run QEMU on Ubuntu 24.04 with an SDL or GTK+ frontend is currently
> unknown. Thus, the X11 example below does not work as it uses and needs
> the `gl sdl` feature set.

Start QEMU with the image previously built. Here, an example for *mgba*.

```
kas shell -c "runqemu kvm slirp serial gl sdl audio" meta-emulator/kas/mgba-x11-kirkstone.yaml
```

Login as user *root* and and start `mgba`.

```
DISPLAY=:0 mgba /usr/share/games/gameboy/flappyboy.gb
```

**Note:** If starting of QEMU fails because _package dri was not found_ you need to install the MESA development package of your distribution. For Ubuntu `sudo apt install mesa-common-dev` does the trick.
